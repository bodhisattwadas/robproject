<?php
session_start();
require_once 'connectInit.php';
error_reporting(E_ALL);

ini_set('display_errors', '1');
ini_set("soap.wsdl_cache_enabled", 0);
$client = new soapclient("https://webservice.betersys.nl/Webservices/serverquestionlists.php?wsdl", array('cache_wsdl' => WSDL_CACHE_NONE));
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <?php 
        $CSSClient = $client->GetQuestionListCSS($BeterSysWachtwoord, $_SESSION['Clinic'], $_SESSION['QuestionList']);
        echo $CSSClient;
        ?>
    </head>
    <style>
        .question{
            font-family: "Times New Roman", Times, serif;
            font-size: 20px;
            margin-bottom: 10px;
        }
        
        .btn-group-vertical > label.btn{
            margin-bottom:10px;
            border-radius:1px !important;
            }
    </style>
    </head>
    
    <body>
        <?php            
            $Welcome = $client->GetQuestionListWelcome($BeterSysWachtwoord, $_SESSION["Clinic"], $_SESSION['QuestionList']);
            $CancelButtonText = $client->GetQuestionListCancel($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["QuestionList"]);
        ?>
        <div class="row">
            <br>
            <div class="col-sm-2"></div>
            <div class="col-sm-8 text-center">
                <?php //echo $CSSClient; ?>
                <br>
                <h2><?php if(isset($_GET['cancel'])) echo $CancelButtonText; else echo $Welcome; ?></h2>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <div class="row">
            <div class="col-sm-5"></div>
            <div class="col-sm-5"></div>
            <div class="col-sm-2">
                <?php 
                //cl=01&pt=10013&ql=1004&qls=1007
                $url = 'questionlistmain.php'.'?cl='.$_SESSION["Clinic"].'&pt='.$_SESSION["Patient"]
                        .'&ql='.$_SESSION["QuestionList"].'&qls='.$_SESSION["QuestionListSent"]; 
                ?>
                <?php if(!isset($_GET['cancel'])) : ?>
                <a href="<?php echo $url; ?>" class="btn btn-primary btn-lg betersys" role="button">Start</a>
                <?php endif; ?>
                
            </div>
        </div>
    </body>
</html>
