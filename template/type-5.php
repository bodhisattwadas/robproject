<!-- Horizontal List -->
<div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="question"><?php echo $qText; ?></div>
                    <?php //print_r($qOptionArray); ?>
                    <div class="btn-group" data-toggle="buttons">
                    <?php foreach ($qOptionArray as $key): ?>
                        <button class="btn btn-primary betersys" onclick="onClickType5('<?php echo $key->QuestionOptionOrder; ?>')"><?php echo $key->QuestionOptionText; ?></button> 
                    <?php endforeach; ?>
                  </div>
                </div>
                <div class="col-sm-3"></div>
</div>
<script>
function onClickType5(optionValue) {
    $.ajax(
                {
                    url : "formSubmit.php",
                    type : "POST",
                    dataType : "json",
                    data : {
                        'submitType5Post' : optionValue
                    },
                    success:function(data)
                    {
                        if(data == 9){
                            window.location.reload();
                        }
                    }
                });
}
</script>

