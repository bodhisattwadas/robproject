<!-- Free text -->
<?php if($questionVariable[0]->IntegerField == 1) : ?>
<?php $defaultValue = $qOptionArray[0]->QuestionOptionDefault ; ?>
<script>
    function onClickType2() {
        minVal = parseFloat(<?php echo $questionVariable[0]->IntegerFieldMinValue; ?>);
        maxVal = parseFloat(<?php echo $questionVariable[0]->IntegerFieldMaxValue; ?>);
        commentValue = parseFloat($("#commentText").val());
        errorMessage = "<?php echo $questionVariable[0]->IntegerFieldErrorMessage; ?>";
        console.log(minVal+' '+maxVal+' '+commentValue);
        if(minVal<=commentValue && commentValue <= maxVal){
            $.ajax(
                    {
                        url : "formSubmit.php",
                        type : "POST",
                        dataType : "json",
                        data : {
                            'submitType2Post' : $("#commentText").val()
                        },
                        success:function(data)
                        {
                            if(data == 9){
                                window.location.reload();
                            }
                        }
                    });
        }else{
            alert(errorMessage);
        }
}
</script>
<?php elseif($questionVariable[0]->DateField == 1) : ?>
<?php 
    $defaultValue = $qOptionArray[0]->QuestionOptionDefault ;
    $d = explode("-",$defaultValue);
    $defaultValue = $d[2].'-'.$d[1].'-'.$d[0];
?>
<script>
    function onClickType2() {
        minVal = "<?php echo $questionVariable[0]->DateFieldMinValue; ?>";
        maxVal = "<?php echo $questionVariable[0]->DateFieldMaxValue; ?>";
        commentValue = $("#commentText").val();
        errorMessage = "<?php echo $questionVariable[0]->DateFieldErrorMessage; ?>";
        
        if(moment(commentValue,'DD-MM-YYYY',true).isValid()){
                console.log("valid date");
                compareDate = moment(commentValue, "DD-MM-YYYY",true);
                startDate   = moment(minVal, "YYYY-MM-DD",true);
                endDate     = moment(maxVal, "YYYY-MM-DD",true);
                if(compareDate.isBetween(startDate, endDate)){
                    $.ajax(
                    {
                        url : "formSubmit.php",
                        type : "POST",
                        dataType : "json",
                        data : {
                            'submitType2Post' : commentValue
                        },
                        success:function(data)
                        {
                            if(data == 9){
                                window.location.reload();
                            }
                        }
                    });
                }else{
                    alert(errorMessage);
                }
            }else{
                    alert(errorMessage);
            }
}
</script>
<?php else : ?>
<?php $defaultValue = $qOptionArray[0]->QuestionOptionText ; ?>
<script>
function onClickType2() {
    $.ajax(
                {
                    url : "formSubmit.php",
                    type : "POST",
                    dataType : "json",
                    data : {
                        'submitType2Post' : $("#commentText").val()
                    },
                    success:function(data)
                    {
                        if(data == 9){
                            window.location.reload();
                        }
                    }
                });
}
</script>
<?php endif; ?>
<?php 
if($questionVariable[0]->FieldNumberOfLines == '') 
    $height = 1; 
else $height = $questionVariable[0]->FieldNumberOfLines; 
?>
<div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="question"><?php echo $qText; ?></div>
                    <textarea class="form-control" rows="<?php echo $height; ?>" id="commentText" value="<?php echo $defaultValue; ?>" placeholder="<?php echo $defaultValue; ?>"></textarea>
                 </div>
                <div class="col-sm-3"></div>
            </div>
