<!-- Vertical List -->
<div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="question"><?php echo $qText; ?></div>
                    <div class="btn-group-vertical" data-toggle="buttons">
                    <?php foreach ($qOptionArray as $key): ?>
                    <button class="btn btn-primary betersys" onclick="onClickType3('<?php echo $key->QuestionOptionOrder; ?>')"><?php echo $key->QuestionOptionText; ?></button> 
                    <?php endforeach; ?>
                  </div>
                </div>
                <div class="col-sm-3"></div>
</div>
<script>
function onClickType3(optionValue) {
    $.ajax(
                {
                    url : "formSubmit.php",
                    type : "POST",
                    dataType : "json",
                    data : {
                        'submitType3Post' : optionValue
                    },
                    success:function(data)
                    {
                        if(data == 9){
                            window.location.reload();
                        }
                    }
                });
}
</script>