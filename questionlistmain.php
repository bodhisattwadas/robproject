<?php
session_start();
require_once 'connectInit.php';
error_reporting(E_ALL);

ini_set('display_errors', '1');
ini_set("soap.wsdl_cache_enabled", 0);
$client = new soapclient("https://webservice.betersys.nl/Webservices/serverquestionlists.php?wsdl", array('cache_wsdl' => WSDL_CACHE_NONE));
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://momentjs.com/downloads/moment.js"></script>
        <?php 
        $CSSClient = $client->GetQuestionListCSS($BeterSysWachtwoord, $_SESSION['Clinic'], $_SESSION['QuestionList']);
        echo $CSSClient;
        ?>
    </head>
    <style>
        .question{
            font-family: "Times New Roman", Times, serif;
            font-size: 20px;
            margin-bottom: 10px;
        }
        
        .btn-group-vertical > .btn{
            margin-bottom:10px;
            border-radius:1px !important;
            }
        .btn-group > .btn{
            margin-right:5px;
            border-radius:1px !important;
            } 
         textarea {
            resize: none;
         }
         .custom {
                width: 78px !important;
            }
    </style>
    
    </head>
    
    <body>
        
        <?php
            $CSSClient = $client->GetQuestionListCSS($BeterSysWachtwoord, $_SESSION['Clinic'], $_SESSION['QuestionList']);
            $Welcome = $client->GetQuestionListWelcome($BeterSysWachtwoord, $_SESSION["Clinic"], $_SESSION['QuestionList']);
            $questionNext = $client->GetQuestionNextNumber($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["Patient"],$_SESSION["QuestionList"],$_SESSION["QuestionListSent"]);
            
            //$questionNext = '10027';
            //echo $questionNext;
            $_SESSION['QuestionNumber'] = $questionNext;
            $questionType = $client->GetQuestionType($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["Patient"],$_SESSION["QuestionList"],$questionNext);
            //print_r($client->GetQuestionVariables($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["QuestionList"],$questionNext));
            
        ?>
        <div class="row">
            <br>
            <div class="col-sm-2"></div>
            <div class="col-sm-8 text-center">
                <br>
                <?php if($questionType == '1') :?>
                <?php 
                $qText = $client->GetQuestionText($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["Patient"],$_SESSION["QuestionList"],'0',$questionNext);
                include 'template/type-1.php'; 
                ?>
                
                <?php elseif($questionType == '2') :?>
                <?php 
                $qText = $client->GetQuestionText($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["Patient"],$_SESSION["QuestionList"],'0',$questionNext);
                $questionVariable = $client->GetQuestionVariables($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["QuestionList"],$questionNext);
                $qOptionArray = $client->GetQuestionOptions($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["Patient"],$_SESSION["QuestionList"],$questionNext);
                //print_r($qOptionArray);
                include 'template/type-2.php'; 
                ?>
                
                <?php elseif($questionType == '3') :?>
                <?php 
                $qText = $client->GetQuestionText($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["Patient"],$_SESSION["QuestionList"],'0',$questionNext);
                $qOptionArray = $client->GetQuestionOptions($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["Patient"],$_SESSION["QuestionList"],$questionNext);
                //print_r($qOptionArray);
                include 'template/type-3.php'; 
                ?>
                
                <?php elseif($questionType == '5') :?>
                <?php 
                $qText = $client->GetQuestionText($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["Patient"],$_SESSION["QuestionList"],'0',$questionNext);
                $qOptionArray = $client->GetQuestionOptions($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["Patient"],$_SESSION["QuestionList"],$questionNext);
                include 'template/type-5.php'; 
                ?>
                <?php endif; ?>
                
                <?php if($questionNext == '0') :?>
                <?php 
                $qText = $client->GetQuestionListThankYou($BeterSysWachtwoord,$_SESSION['Clinic'],$_SESSION["QuestionList"]);
                echo '<h2>'.$qText.'</h2>';
                ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-2"></div>
        </div>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4 text-center">
                
            </div>
            <?php 
                //cl=01&pt=10013&ql=1004&qls=1007
                $url = 'index.php'.'?cl='.$_SESSION["Clinic"].'&pt='.$_SESSION["Patient"]
                        .'&ql='.$_SESSION["QuestionList"].'&qls='.$_SESSION["QuestionListSent"].'&cancel'; 
                //$cancelRedirect = 'index.php?cancel';
            ?>
            <div class="col-sm-4 text-center">
                <?php if($questionType == '2') : ?>
                <button type="button" class="btn btn-success custom betersys" onclick="onClickType2()">Opslaan</button>
                <?php endif; ?>
                <?php if($questionNext != '0') :?>
                <!-- Cancel button -->
                <a type="button" class="btn btn-danger custom" href="<?php echo $url; ?>" title="Tijdelijk onderbreken invullen vragenlijst, gebruik de link in de uitnodigingsmail om verder te gaan">Afbreken</a>
                <?php endif; ?>
            </div>
        </div>
    </body>
</html>